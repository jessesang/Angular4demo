import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {Product} from "../di/product-info/product-info.component";
import {Injectable} from "@angular/core";
/**
 * Created by Administrator on 2017/7/28.
 */

@Injectable()
export  class ProductResolve implements Resolve<Product> {

  constructor(private router: Router) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let productId: number = route.params['id'];
    if (productId == 12) {
       return new Product(1, 'iphone7');
    } else {
       this.router.navigate(['/home'])
    }
    return undefined;
  }

}
