import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

    a: number ;
  constructor(private  router: Router) { }

  toDetails() {
    console.log("-------------------2>");
    this.router.navigate(['/detail', 110]);
  }

  ngOnInit() {
    this.a = 6;
  }

}
