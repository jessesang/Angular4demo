import { Injectable } from '@angular/core';

@Injectable()
export class MockHeroService {
  heros: Array<{ id: number; name: string }> = [
    { id: 16, name: 'Mock_RubberMan1' },
    { id: 17, name: 'Mock_Dynama' },
    { id: 18, name: 'Mock_Dr IQ111' },
    { id: 19, name: 'Mock_Magmadddd' },
    { id: 20, name: 'Mock_Tornado' }
  ];

  getHeros() {
    return this.heros;
  }
  constructor() { }

}
