import {News2Component} from "../../news2/news2.component";
import {NewsComponentComponent} from "../../news-component/news-component.component";
import {HomeComponent} from "../../home/home.component";
import {MainComponent} from "../main/main.component";
import {ProductComponent} from "../../product/product.component";
import {ProductDescComponent} from "../../product-desc/product-desc.component";
import {SellerInfoComponent} from "../../seller-info/seller-info.component";
import {LoginGuard} from "../../guard/login.guard";
import {UnsavedGuard} from "../../guard/unsaved.guard";
import {ProductInfoComponent} from "../product-info/product-info.component";
import {ProductResolve} from "../../guard/product.resolve";
/**
 * Created by Administrator on 2017/7/27.
 */


export const diRoutes = [

      // 以后在这里改动配置,
      {
        path: '',
      component: MainComponent,
        children: [
          { path: '', redirectTo: 'product/2', pathMatch: 'full'},
//  { path: 'diMain',  component: MainComponent },
      // path:路径 /news component:组件
      { path: 'news',  component: NewsComponentComponent },
      { path: 'news2',  component: News2Component },
      // 懒加载子模块, 子模块需要配置路由设置启动子组件
      // { path: 'other', loadChildren:"./other/other.module#otherModule" },

      // { path: 'news2', outlet: 'let1', component: NewsComponentComponent },
      { path: 'news2', outlet: 'let2' , component: News2Component },
      {path: 'home', component: HomeComponent},
      // {path: 'product/2/chat', component: News2Component, outlet: 'aux'},
  {path: 'product/:id', component: ProductComponent, children : [
    { path: '', component: ProductDescComponent},
    {path: 'seller/:id', component: SellerInfoComponent}] // , canActivate: [LoginGuard], canDeactivate: [UnsavedGuard]
  },
   {path: 'product2/:id', component: ProductComponent, children : [
            { path: '', component: ProductDescComponent},
            {path: 'seller/:id', component: SellerInfoComponent}]  , canActivate: [LoginGuard], canDeactivate: [UnsavedGuard]
    },
     {
        path: 'resolveProduct/:id', component: ProductInfoComponent, resolve: {product: ProductResolve}
     }

// 重定向
     // { path: '**', pathMatch: 'full', redirectTo: '/home' }, // 调换这个顺序 影响路由的匹配
]}
];
