import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {diRoutes} from "./di.routes";
import {RouterModule} from "@angular/router";
import {MainComponent} from "../main/main.component";
import {News2Component} from "../../news2/news2.component";
import {NewsComponentComponent} from "../../news-component/news-component.component";
import {HomeComponent} from "../../home/home.component";
import {SellerInfoComponent} from "../../seller-info/seller-info.component";
import {ProductDescComponent} from "../../product-desc/product-desc.component";
import {ProductComponent} from "../../product/product.component";
import {ProductInfoComponent} from "../product-info/product-info.component";
import {ProductResolve} from "../../guard/product.resolve";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(diRoutes)
  ],
  exports: [
    MainComponent
  ],
  declarations: [NewsComponentComponent, HomeComponent, MainComponent, ProductInfoComponent, News2Component,
                  ProductComponent, ProductDescComponent, SellerInfoComponent],
  providers: [ProductResolve]
})
export class DiModule { }
