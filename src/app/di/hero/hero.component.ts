import { Component, OnInit } from '@angular/core';
import {HeroService} from "../hero.service";
import {LoggerService} from "../logger.service";

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css'],
 /* //providers: [HeroService]*/
  // providers: [HeroService, LoggerService]
})
export class HeroComponent implements OnInit {
  heros: Array<{ id: number; name: string }>;
  constructor(private heroService: HeroService,
              private loggerService: LoggerService) { }

  ngOnInit() {
   /* this.heros = [
      { id: 11, name: 'Mr. Nice' },
      { id: 12, name: 'Narco' },
      { id: 13, name: 'Bombasto' },
      { id: 14, name: 'Celeritas' },
      { id: 15, name: 'Magneta' }
    ];*/
   // this.heros = this.heroService.getHeros();
   //  this.loggerService = new LoggerService(true);
    this.loggerService.log('Fetching heros.www..');
    this.heros = this.heroService.getHeros();
  }

}
