import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css']
})
export class ProductInfoComponent implements OnInit {
   productId: number;
   productName: string;

  constructor(private  routeInfo: ActivatedRoute) { }

  ngOnInit() {
    this.routeInfo.data.subscribe((data: {product: Product}) => {
          this.productId = data.product.id;
          this.productName = data.product.name;
    });
  }

}

export  class  Product {
  constructor(public  id: number, public name: string){}
}
