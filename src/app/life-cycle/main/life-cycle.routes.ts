import {MainComponent} from "./main.component";
/**
 * Created by Administrator on 2017/8/11.
 */
export const lifeCycleRoutes = [{
  path: '',
  component: MainComponent,
  // canActivate: [AuthGuard],
  children: [
    { path: '', redirectTo: 'main', pathMatch: 'full'},
    { path: 'main', component: MainComponent },
  ]
}]
