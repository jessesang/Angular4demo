import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MainComponent} from "./main.component";
import {PeekABooComponent} from "../peek-a-boo/peek-a-boo.component";
import {PeekAParentComponent} from "../peek-a-parent/peek-a-parent.component";
import {RouterModule} from "@angular/router";
import {lifeCycleRoutes} from "./life-cycle.routes";
import {PanelDirective} from "../panel.directive";
import {FormsModule} from "@angular/forms";
import {ChildComponent} from "../child/child.component";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(lifeCycleRoutes)
  ],
  declarations: [MainComponent, PeekABooComponent, PeekAParentComponent, PanelDirective, ChildComponent]
})
export class LifeCycleModule { }
