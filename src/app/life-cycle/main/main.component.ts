import { Component, OnInit } from '@angular/core';
import {PanelDirective} from "../panel.directive";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  counter: number = 0;
  greeting: string = 'hello';
  user: {name: string} = {name: 'Tom'};
  constructor() { }
  ngOnInit() {
  }
  toggle() {
    this.counter += 1;
  }
}
