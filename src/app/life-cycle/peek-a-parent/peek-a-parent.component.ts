import { Component, OnInit } from '@angular/core';
import {LoggerService} from "../logger.service";

@Component({
  selector: 'app-peek-a-parent',
  templateUrl: './peek-a-parent.component.html',
  styleUrls: ['./peek-a-parent.component.css'],
  providers:  [ LoggerService ]
})
export class PeekAParentComponent implements OnInit {
  hasChild = false;
  hookLog: string[];

  heroName = 'Windstorm';
  private logger: LoggerService;
  constructor(logger: LoggerService) {
    this.logger = logger;
    this.hookLog = logger.logs;
  }
  toggleChild() {
    this.hasChild = !this.hasChild;
    if (this.hasChild) {
      this.heroName = 'Windstorm';
      this.logger.clear(); // clear log on create
    }
    this.logger.tick();
  }

  updateHero() {
    // this.heroName += '!';
    //  this.logger.tick();
  }
  ngOnInit() {
  }

}
