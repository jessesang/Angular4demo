import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeekAParentComponent } from './peek-a-parent.component';

describe('PeekAParentComponent', () => {
  let component: PeekAParentComponent;
  let fixture: ComponentFixture<PeekAParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeekAParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeekAParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
