import {
  AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, Directive, DoCheck, Input, OnChanges,
  OnDestroy,
  OnInit
} from '@angular/core';

@Directive({
  selector: '[appPanel]',
})
export class PanelDirective implements  OnInit, OnChanges, OnDestroy, DoCheck, AfterContentInit,
  AfterViewInit, AfterContentChecked, AfterViewChecked {
  @Input() title: string;
  @Input() caption2: string;
  constructor() { }

  // 组件输入属性值发生改变（首次调用一定会发生在 ngOnInit之前。）
  ngOnChanges(changes) {
    console.log('On changes', changes);
  }
  // 组件初始化完成（在第一轮 ngOnChanges 完成之后调用。
  //  ( 译注：也就是说当每个输入属性的值都被触发了一次 ngOnChanges 之后才会调用 ngOnInit ，此时所有输入属性都已经有了正确的初始绑定值 )）
  ngOnInit() {
    console.log('Initialized');
    console.warn('OnChanges和DoCheck钩子不要同时出现，互斥！。本例仅供学习');
  }
  // 脏值检测器被调用后调用
  ngDoCheck() {
    console.log('Do check');
  }
  // 组件销毁之前
  ngOnDestroy() {
    console.log('Destroy');
  }
  // 组件-内容-初始化完成 PS：指的是ContentChild或者Contentchildren
  ngAfterContentInit() {
    console.log('After content init');
  }
  // 组件内容脏检查完成
  ngAfterContentChecked() {
    console.log('After content checked');
  }
  // 组件视图初始化完成 PS：指的是ViewChild或者ViewChildren
  ngAfterViewInit() {
    console.log('After view init');
  }
  // 组件视图脏检查完成之后
  ngAfterViewChecked() {
    console.log('After view checked');
  }
}
