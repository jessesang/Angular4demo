import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDirective2Component } from './create-directive2.component';

describe('CreateDirective2Component', () => {
  let component: CreateDirective2Component;
  let fixture: ComponentFixture<CreateDirective2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDirective2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDirective2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
