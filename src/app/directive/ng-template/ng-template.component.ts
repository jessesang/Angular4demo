import {AfterViewInit, Component, OnInit, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-ng-template',
  templateUrl: './ng-template.component.html',
  styleUrls: ['./ng-template.component.css']
})
export class NgTemplateComponent implements OnInit, AfterViewInit {
  @ViewChild('tpl')
  tplRef: TemplateRef<any>;
  context = { message: 'Hello ngOutletContext!',
    $implicit: 'Hello, Semlinker!' };

  constructor(private vcRef: ViewContainerRef) {}

  ngOnInit(): void {
  }
  ngAfterViewInit() {
    this.vcRef.createEmbeddedView(this.tplRef);
  }

}
