import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorDirectiveComponent } from './author-directive.component';

describe('AuthorDirectiveComponent', () => {
  let component: AuthorDirectiveComponent;
  let fixture: ComponentFixture<AuthorDirectiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorDirectiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorDirectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
