import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDirectiveComponent } from './event-directive.component';

describe('EventDirectiveComponent', () => {
  let component: EventDirectiveComponent;
  let fixture: ComponentFixture<EventDirectiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDirectiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDirectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
