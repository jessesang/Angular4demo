import {AfterViewInit, Component, ElementRef, OnInit, Renderer, ViewChild} from '@angular/core';
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-main-directive2',
  templateUrl: './main-directive2.component.html',
  styleUrls: ['./main-directive2.component.css']
})
export class MainDirective2Component implements OnInit , AfterViewInit {
  collection: {
    id: number;
  }[];
  cities: string[];
  auth: Observable<{}>;
  @ViewChild('myInput') input: ElementRef;
  constructor(private renderer: Renderer) { }

  ngOnInit() {
    this.cities = ['1', '2', '5', '4'];
    this.collection = [{id: 1}, {id: 2}, {id: 3}];

    this.auth = Observable
      .of({ username: 'semlinker', password: 'segmentfault' })
      .delay(new Date(Date.now() + 2));
  }
  getItems() {
    this.collection = this.getItemsFromServer();
  }
  getItemsFromServer() {
    return [{id: 1}, {id: 2}, {id: 3}, {id: 4}];
  }
  trackByFn(index, item) {
    return index; // or item.id
  }

  ngAfterViewInit() {
    this.renderer.invokeElementMethod(
      this.input.nativeElement, 'focus');
  }
}
