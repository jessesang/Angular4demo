import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainDirective2Component } from './main-directive2.component';

describe('MainDirective2Component', () => {
  let component: MainDirective2Component;
  let fixture: ComponentFixture<MainDirective2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainDirective2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainDirective2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
