import {CreateDirectiveComponent} from "./create-directive/create-directive.component";
import {MainDirective2Component} from "./main-directive2/main-directive2.component";
import {CreateDirective2Component} from "./create-directive2/create-directive2.component";
import {InputDirectiveComponent} from "./input-directive/input-directive.component";
import {EventDirectiveComponent} from "./event-directive/event-directive.component";
import {AuthorDirectiveComponent} from "./author-directive/author-directive.component";
import {NgTemplateComponent} from "./ng-template/ng-template.component";
import {NgIfComponent} from "./ng-if/ng-if.component";
import {StructuralDirectivesComponent} from "./structural-directives/structural-directives.component";
/**
 * Created by Administrator on 2017/8/1.
 */
export const directiveRoutes = [
  {
    path: '',
    component: MainDirective2Component,
    // canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'create', pathMatch: 'full'},
      { path: 'create', component: CreateDirectiveComponent },
      { path: 'create2', component: CreateDirective2Component },
      { path: 'input',  component: InputDirectiveComponent },
      { path: 'event',   component: EventDirectiveComponent },
      { path: 'author',   component: AuthorDirectiveComponent },
      { path: 'ngTemplate',   component: NgTemplateComponent },
      { path: 'ngIf',   component: NgIfComponent },
      { path: 'structDirectives',   component: StructuralDirectivesComponent },
    ]
  }
];
