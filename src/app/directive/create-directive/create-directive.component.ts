import {Component, HostBinding, OnInit} from '@angular/core';

@Component({
  selector: 'app-create-directive',
  templateUrl: './create-directive.component.html',
  styleUrls: ['./create-directive.component.css']
})
export class CreateDirectiveComponent implements OnInit {
  @HostBinding() innerText = 'Hello, Everyone!';
  constructor() { }

  ngOnInit() {
  }

}
