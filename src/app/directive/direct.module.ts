import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CreateDirectiveComponent} from "./create-directive/create-directive.component";
import { MainDirective2Component } from './main-directive2/main-directive2.component';
import {directiveRoutes} from "./direct.router";
import {RouterModule} from "@angular/router";
import {Highlight2Directive} from "./attribute-directives/highlight2.directive";
import { CreateDirective2Component } from './create-directive2/create-directive2.component';
import { InputDirectiveComponent } from './input-directive/input-directive.component';
import {GreetDirectiveDirective} from "./attribute-directives/greet-directive.directive";
import { GreetDirectiveEventDirective } from './attribute-directives/greet-directive-event.directive';
import { EventDirectiveComponent } from './event-directive/event-directive.component';
import { GreetDirectiveAuthorDirective } from './attribute-directives/greet-directive-author.directive';
import { AuthorDirectiveComponent } from './author-directive/author-directive.component';
import { NgTemplateComponent } from './ng-template/ng-template.component';
import { NgIfComponent } from './ng-if/ng-if.component';
import { UnlessDirectiveDirective } from './attribute-directives/unless-directive.directive';
import { StructuralDirectivesComponent } from './structural-directives/structural-directives.component';
import {FormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";
import {heroSwitchComponents} from "./structural-directives/hero-switch.components";
import { UnlessDirective } from './unless.directive';

@NgModule({
  imports: [
    // BrowserModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild(directiveRoutes)
  ],
  declarations: [CreateDirectiveComponent, MainDirective2Component, Highlight2Directive, CreateDirective2Component,
      InputDirectiveComponent, GreetDirectiveDirective, GreetDirectiveEventDirective, EventDirectiveComponent,
    GreetDirectiveAuthorDirective, AuthorDirectiveComponent, NgTemplateComponent, NgIfComponent,
    UnlessDirectiveDirective, StructuralDirectivesComponent, heroSwitchComponents, UnlessDirective]
})
export class DirectModule { }
