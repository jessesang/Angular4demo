import {Directive, HostBinding, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appGreetDirectiveEvent]'
})
export class GreetDirectiveEventDirective {
  @Input() appGreetDirectiveEvent: string;

  @HostBinding() get innerText() {
    return this.appGreetDirectiveEvent;
  }

  @HostListener('click',  ['$event'])
  onClick(event) {
    this.appGreetDirectiveEvent = 'Clicked!';
  }
  constructor() { }

}
