import {Attribute, Directive, HostBinding, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appGreetDirectiveAuthor]'
})
export class GreetDirectiveAuthorDirective {
  @Input() appGreetDirectiveAuthor: string;

  @HostBinding() get innerText() {
    return this.appGreetDirectiveAuthor;
  }

  @HostListener('click', ['$event'])
  onClick(event) {
    this.appGreetDirectiveAuthor = 'Clicked!';
    console.dir(event);
  }

  constructor(@Attribute('author') public author: string) {
    this.appGreetDirectiveAuthor = 'Clicked!' + author;
    console.log(author);
  }

}
