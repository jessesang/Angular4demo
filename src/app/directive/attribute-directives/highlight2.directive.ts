import {Directive, ElementRef} from '@angular/core';

@Directive({ selector: '[myHighlight]' })
export class Highlight2Directive {
  constructor(el: ElementRef) {
    el.nativeElement.style.backgroundColor = 'yellow';
  }
}
