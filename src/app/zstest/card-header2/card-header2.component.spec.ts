import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardHeader2Component } from './card-header2.component';

describe('CardHeader2Component', () => {
  let component: CardHeader2Component;
  let fixture: ComponentFixture<CardHeader2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardHeader2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardHeader2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
