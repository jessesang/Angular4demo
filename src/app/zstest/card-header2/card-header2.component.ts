import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-card-header2',
  templateUrl: './card-header2.component.html',
  styleUrls: ['./card-header2.component.css']
})
export class CardHeader2Component implements OnInit {
  @Input()
  title: string;
  constructor() { }

  ngOnInit() {
    this.title = "2eqeqwe";
  }
}
