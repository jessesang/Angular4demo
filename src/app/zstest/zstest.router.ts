import {MainNavAndContentComponent} from "./main-nav-and-content/main-nav-and-content.component";
import {ParentOrder2Component} from "./parent-order2/parent-order2.component";
import {MainComponent} from "./main/main.component";
import {DemoInput2Component} from "./demo-input2/demo-input2.component";
import {Templateform2Component} from "./templateform2/templateform2.component";
import {ReactiveForm2Component} from "./reactive-form2/reactive-form2.component";
/**
 * Created by Administrator on 2017/7/30.
 */
export const zstestRoutes = [

  // 以后在这里改动配置,
  {
    path: '',
    component: MainNavAndContentComponent,
    children: [
      { path: '', redirectTo: 'input1', pathMatch: 'full'},
//  { path: 'diMain',  component: MainComponent },
      // path:路径 /news component:组件
      { path: 'input1',  component: ParentOrder2Component },
      { path: 'input2',  component: DemoInput2Component },
      { path: 'output1',  component: MainComponent },
     {path:'formtest', component:Templateform2Component},
     {path:'reactiveform', component:ReactiveForm2Component}
// 重定向
      // { path: '**', pathMatch: 'full', redirectTo: '/home' }, // 调换这个顺序 影响路由的匹配
    ]}
];
