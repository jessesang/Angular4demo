import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardFooter2Component } from './card-footer2.component';

describe('CardFooter2Component', () => {
  let component: CardFooter2Component;
  let fixture: ComponentFixture<CardFooter2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardFooter2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardFooter2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
