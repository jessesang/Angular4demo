import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-card-footer2',
  templateUrl: './card-footer2.component.html',
  styleUrls: ['./card-footer2.component.css']
})
export class CardFooter2Component implements OnInit {
  @Output() onChange: EventEmitter<string> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  onBlur(value: string) {

    this.onChange.emit(value); // 传播事件

  }
}
