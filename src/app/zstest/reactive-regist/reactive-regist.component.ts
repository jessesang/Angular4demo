import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {equalValidator, mobileAsyncValidator, mobileValidator} from "../validator/validators";

@Component({
  selector: 'app-reactive-regist',
  templateUrl: './reactive-regist.component.html',
  styleUrls: ['./reactive-regist.component.scss']
})
export class ReactiveRegistComponent implements OnInit {

  // xxxxxx(control:AbstractControl): {[key:string]: any}


  formModel: FormGroup;

  constructor(fb: FormBuilder) {
   /* this.formModel = new FormGroup({
       username: new FormControl(),
       mobile: new FormControl(),
       passwordsGroup: new FormGroup({
         password: new FormControl(),
        pconfirm: new FormControl()
      })
    })*/
   this.formModel = fb.group({
     username: ['', [Validators.required, Validators.minLength(6)]],
     mobile: ['', mobileValidator, mobileAsyncValidator],
     passwordsGroup: fb.group({
       password: [''],
       pconfirm: ['']
     }, {validator: equalValidator})
   })
  }

  ngOnInit() {
  }
  onSubmit() {
    // let isValid: boolean = this.formModel.get("username").valid;
    // console.log("username的校验结果：" + isValid);
    // let errors: any = this.formModel.get("username").errors;
    // console.log("username的错误信息是：" + JSON.stringify(errors));
    if (this.formModel.valid) {
      console.log(this.formModel.value);
    }
   // console.log(this.formModel.value);
  }

 /* mobileValidator(control: FormControl): any {
    var mPattern = /^1[34578]\d{9}$/;
    let valid = mPattern.test(control.value);
    console.log("mobile的校验结果是：" + valid);
    return valid ? null : {mobile : true}
  }

  equalValidator(group: FormGroup): any {
      let password: FormControl = group.get("password")  as FormControl;
      let pconfirm: FormControl = group.get("pconfirm") as FormControl;
      let valid: boolean = (password.value === pconfirm.value);
      console.log("密码校验结果：" + valid);
      return valid ? null : {equal:true};
  }*/
}
