import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoInput2Component } from './demo-input2.component';

describe('DemoInput2Component', () => {
  let component: DemoInput2Component;
  let fixture: ComponentFixture<DemoInput2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoInput2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoInput2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
