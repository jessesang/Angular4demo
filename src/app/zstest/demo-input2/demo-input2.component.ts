import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-demo-input2',
  templateUrl: './demo-input2.component.html',
  styleUrls: ['./demo-input2.component.css']
})
export class DemoInput2Component implements OnInit {
  @Input()
  title: string
  // @Input('title')
  constructor() { }

  ngOnInit() {
    this.title = "222";
  }

  onChange(event: any) {

    alert(event);

  }

}
