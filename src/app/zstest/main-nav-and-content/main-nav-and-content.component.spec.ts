import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainNavAndContentComponent } from './main-nav-and-content.component';

describe('MainNavAndContentComponent', () => {
  let component: MainNavAndContentComponent;
  let fixture: ComponentFixture<MainNavAndContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainNavAndContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainNavAndContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
