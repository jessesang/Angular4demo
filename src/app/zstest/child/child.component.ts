import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'free-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit, OnChanges {

  @Input()
  greeting: string; // greeting为字符串(不可变)，user为对象(可变)

  @Input()
  user: {name: string};

  message: string = '初始化消息';

  ngOnChanges(changes: SimpleChanges): void {
    console.log(JSON.stringify(changes, null, 2));
  }


  constructor() { }

  ngOnInit() {
  }

}
