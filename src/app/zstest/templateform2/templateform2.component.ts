import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-templateform2',
  templateUrl: './templateform2.component.html',
  styleUrls: ['./templateform2.component.css']
})
export class Templateform2Component implements OnInit {
  mobileValid: boolean = true;
  mobileUntouched: boolean = true;
  constructor() { }

  ngOnInit() {
  }
  onSubmint(value: any, valid: boolean) {
    console.log(value);
    console.log(valid)
  }

  onMobileInput(form: NgForm)  {
    console.log("------------------/>");
    if (form) {
      this.mobileValid = form.form.get("mobile").valid;
      this.mobileUntouched = form.form.get("mobile").untouched;
    }
  }
}
