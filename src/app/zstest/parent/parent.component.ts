import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'free-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss']
})
export class ParentComponent implements OnInit {

  greeting: string = 'hello';
  user: {name: string} = {name: 'Tom'};

  constructor() { }

  ngOnInit() {
  }

}
