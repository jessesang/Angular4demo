import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-price-quote2',
  templateUrl: './price-quote2.component.html',
  styleUrls: ['./price-quote2.component.css']
})
export class PriceQuote2Component implements OnInit {

  stockCode: string = 'IBM';
  price: number;

  // 使用EventEmitter发射事件
  // 泛型是指往外发射的事件是什么类型
  // priceChange为事件名称
  @Output()
  priceChange: EventEmitter<PriceQuote> = new EventEmitter();

  constructor() {
    setInterval(() => {
      let priceQuote = new PriceQuote(this.stockCode, 100*Math.random());
      this.price = priceQuote.lastPrice;
      // 发射事件
      this.priceChange.emit(priceQuote);
    })
  }

  ngOnInit() {
  }

}
// 股票信息类
// stockCode为股票代码， lastPrice为股票价格
export class PriceQuote {
  constructor (public stockCode: string,
               public lastPrice: number
  ) {}

}
