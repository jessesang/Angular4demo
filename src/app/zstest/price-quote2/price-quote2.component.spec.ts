import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceQuote2Component } from './price-quote2.component';

describe('PriceQuote2Component', () => {
  let component: PriceQuote2Component;
  let fixture: ComponentFixture<PriceQuote2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PriceQuote2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceQuote2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
