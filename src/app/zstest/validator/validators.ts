import {FormControl, FormGroup} from "@angular/forms";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
/**
 * Created by Administrator on 2017/7/17.
 */
export function mobileValidator(control: FormControl): any {
  var mPattern = /^1[34578]\d{9}$/;
  let valid = mPattern.test(control.value);
  console.log("mobile的校验结果是：" + valid);
 return valid ? null : {mobile : true}
 //  return Observable.of(valid ? null : {mobile : true}).delay(5000);
}

export function mobileAsyncValidator(control: FormControl): any {
  var mPattern = /^1[34578]\d{9}$/;
  let valid = mPattern.test(control.value);
  console.log("mobile的校验结果是：" + valid);
  // return valid ? null : {mobile : true}
  return Observable.of(valid ? null : {mobile : true}).delay(5000);

}
export function equalValidator(group: FormGroup): any {
  let password: FormControl = group.get("password")  as FormControl;
  let pconfirm: FormControl = group.get("pconfirm") as FormControl;
  let valid: boolean = (password.value === pconfirm.value);
  console.log("密码校验结果：" + valid);
  return valid ? null : {equal: {descxxx: "密码和确认不匹配1！！"}};
}
