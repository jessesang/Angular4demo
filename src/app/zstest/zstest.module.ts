import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { MainNavAndContentComponent } from './main-nav-and-content/main-nav-and-content.component';
import { PriceQuote2Component } from './price-quote2/price-quote2.component';
import {RouterModule} from "@angular/router";
import {zstestRoutes} from "./zstest.router";
// import {ParentOrderComponent} from "./parent-order/parent-order.component";
// import {OrderComponent} from "./order/order.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { Order2Component } from './order2/order2.component';
import { ParentOrder2Component } from './parent-order2/parent-order2.component';
import { CardFooter2Component } from './card-footer2/card-footer2.component';
import { CardHeader2Component } from './card-header2/card-header2.component';
import { DemoInput2Component } from './demo-input2/demo-input2.component';
import { Templateform2Component } from './templateform2/templateform2.component';
import { ReactiveForm2Component }  from './reactive-form2/reactive-form2.component';
import {EqualValidatorDirective} from "./equal-validator.directive";
import {MobileValidatorDirective} from "./fmobile-validator.directive";
import {CardComponent} from "./card/card.component";
import {ChildComponent} from "./child/child.component";
import {FormtestComponent} from "./formtest/formtest.component";
import {ParentComponent} from "./parent/parent.component";
import {ParentPriceQuoteComponent} from "./parent-price-quote/parent-price-quote.component";
import {ReactiveRegistComponent} from "./reactive-regist/reactive-regist.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(zstestRoutes)
  ],
  declarations: [MainComponent, MainNavAndContentComponent, PriceQuote2Component, EqualValidatorDirective, MobileValidatorDirective, CardComponent,
           Order2Component, ParentOrder2Component, CardFooter2Component, CardHeader2Component,
    DemoInput2Component, ChildComponent, FormtestComponent, ParentComponent, ReactiveRegistComponent,
      ParentPriceQuoteComponent,
          Templateform2Component, ReactiveForm2Component]
})
export class ZstestModule { }
