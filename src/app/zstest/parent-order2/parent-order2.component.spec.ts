import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentOrder2Component } from './parent-order2.component';

describe('ParentOrder2Component', () => {
  let component: ParentOrder2Component;
  let fixture: ComponentFixture<ParentOrder2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentOrder2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentOrder2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
