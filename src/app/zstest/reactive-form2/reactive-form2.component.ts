import { Component, OnInit } from '@angular/core';
import {FormArray, FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-reactive-form2',
  templateUrl: './reactive-form2.component.html',
  styleUrls: ['./reactive-form2.component.css']
})
export class ReactiveForm2Component implements OnInit {
  formModel: FormGroup = new FormGroup({
    username: new FormControl(),
    dateRange: new FormGroup({
      from: new FormControl(),
      to:  new FormControl()
    }),
    emails: new FormArray([
      new FormControl("a@aa.com"),
      new FormControl("b@bb.com")
    ])
    /* from: new FormControl(),
     to: new FormControl()*/
  })

  username: FormControl = new FormControl("aaaa");
  constructor() { }

  ngOnInit() {

  }

  onsubmit(){
    console.log(this.formModel.value)
  }

  addEmails() {
    let emials = this.formModel.get("emails") as FormArray;
    emials.push(new FormControl());
  }
}
