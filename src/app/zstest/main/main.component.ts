import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {PriceQuote} from "../price-quote2/price-quote2.component";


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  priceQuote: PriceQuote = new PriceQuote('', 0);

  @Output()
  price2Change: EventEmitter<PriceQuote> = new EventEmitter();

  priceQuoteHandler(event: PriceQuote){
    this.priceQuote = event;
  }

  constructor() { }

  ngOnInit() {
  }

}
