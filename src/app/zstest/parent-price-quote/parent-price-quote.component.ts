import { Component, OnInit } from '@angular/core';
import {PriceQuote} from "../price-quote2/price-quote2.component";

@Component({
  selector: 'free-parent-price-quote',
  templateUrl: './parent-price-quote.component.html',
  styleUrls: ['./parent-price-quote.component.scss']
})
export class ParentPriceQuoteComponent implements OnInit {

  priceQuote: PriceQuote = new PriceQuote('', 0);

  priceQuoteHandler(event: PriceQuote){
    this.priceQuote = event;
  }
  constructor() { }

  ngOnInit() {
  }

}
