import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentPriceQuoteComponent } from './parent-price-quote.component';

describe('ParentPriceQuoteComponent', () => {
  let component: ParentPriceQuoteComponent;
  let fixture: ComponentFixture<ParentPriceQuoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentPriceQuoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentPriceQuoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
