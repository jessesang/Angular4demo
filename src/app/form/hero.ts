/**
 * Created by Administrator on 2017/8/5.
 */
export class Hero {
  constructor(
    public id: number,
    public name: string,
    public power: string,
    public alterEgo?: string  // alterEgo是可选的，调用构造函数时可省略，注意alterEgo?中的问号 (?)。
  ) {  }
}
