import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReaciveForm2Component } from './reacive-form2.component';

describe('ReaciveForm2Component', () => {
  let component: ReaciveForm2Component;
  let fixture: ComponentFixture<ReaciveForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReaciveForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReaciveForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
