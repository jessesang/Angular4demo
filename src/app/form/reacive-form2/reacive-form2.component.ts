import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-reacive-form2',
  templateUrl: './reacive-form2.component.html',
  styleUrls: ['./reacive-form2.component.css']
})
export class ReaciveForm2Component implements OnInit {

  signupForm2: FormGroup;
  signupForm: FormGroup;
  fb: FormBuilder;
  constructor(private router: Router) { }

  ngOnInit() {
    this.signupForm2 = new FormGroup({
      userName: new FormControl('', [Validators.required, Validators.minLength(3)]),
      email: new FormControl('', [Validators.required, Validators.email])
    });

    this.signupForm = this.fb.group({
      userName: 'semlinker',
      hasAddress: false
    });

    this.signupForm = this.fb.group({
      userName: {value: 'semlinker', disabled: true},
      hasAddress: {value: true, disabled: false}
    });

    this.signupForm = this.fb.group({
      userName: [''],
      hasAddress: [{value: true, disabled: false}]
    })
  }

  save() {
    console.log(arguments);
  }
}
