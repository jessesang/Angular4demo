import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-form-main',
  templateUrl: './form-main.component.html',
  styleUrls: ['./form-main.component.css']
})
export class FormMainComponent implements OnInit {
  username = 'semlinker';
  userName = 'semlinker';
  versions = ['1.x', '2.x', '3.x'];
  showGreeting: boolean = true;
  showValue: string;
  value: string;
  hiddenTemplateForm: boolean = true;
  constructor(private  router: Router) { }

  ngOnInit() {
  }
  onSubmit(value) {
    console.dir(value);
  }
  showConent(event) {
    console.log(event)
    this.showValue = "happy1";
  }

  btClick() {
    this.hiddenTemplateForm = !this.hiddenTemplateForm;
    if (this.hiddenTemplateForm) {
        this.router.navigate(['/form3'])
    }
  }

  btClick2() {
    this.router.navigate(['/form4'])
  }
  btClick3() {
    this.router.navigate(['/form5'])
  }

  btClick4() {
    this.router.navigate(['/form6'])
  }
}
