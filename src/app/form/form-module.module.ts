import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormMainComponent} from "./form-main/form-main.component";
import {FormControl, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {formRoutes} from "./form-routes";
import {RouterModule} from "@angular/router";
import { ReaciveForm2Component } from './reacive-form2/reacive-form2.component';
import { TemplateFormComponent } from './template-form/template-form.component';
import { HeroFormComponent } from './hero-form/hero-form.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { HeroListComponent } from './hero-list/hero-list.component';
import { HeroDetail2Component } from './hero-detail2/hero-detail2.component';
import {HeroService} from "./hero.service";
import {BrowserModule} from "@angular/platform-browser";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(formRoutes)
  ],
  declarations: [FormMainComponent, ReaciveForm2Component, TemplateFormComponent, HeroFormComponent, HeroDetailComponent,
    HeroListComponent, HeroDetail2Component],
  providers: [ HeroService ], // <-- #4 provide HeroService
})
export class FormModuleModule { }
