import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import { Address, Hero, states2 } from '../data-model';
@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit, OnChanges {
 // name = new FormControl();
  heroForm = new FormGroup ({
    name: new FormControl()
  });

  heroForm2: FormGroup; // <--- heroForm is of type FormGroup
  heroForm3: FormGroup; // <--- heroForm is of type FormGroup
  states = states2;

  @Input() hero: Hero;
  constructor(private fb: FormBuilder) {
    this.createForm();
  }

  ngOnInit() {
    this.heroForm2 = this.fb.group({
      name: ['', Validators.required ], // <--- the FormControl called "name"
    });
   /* this.heroForm2.setValue({
      name:    this.hero.name,
      address: this.hero.addresses[0] || new Address()
    });

    this.heroForm2.patchValue({
      name: this.hero.name
    });*/
  }

  ngOnChanges() {
    /*this.heroForm2.setValue({
      name:    this.hero.name,
      address: this.hero.addresses[0] || new Address()
    });*/
    this.heroForm.reset({
      name: this.hero.name,
      address: this.hero.addresses[0] || new Address()
    });
  }

  createForm() {
    this.heroForm3 = this.fb.group({
      name: ['', Validators.required ],
      street: '',
      city: '',
      state: '',
      zip: '',
      power: '',
      sidekick: '',
      address: this.fb.group(new Address())
      /*{ // <-- the child FormGroup
        street: '',
        city: '',
        state: '',
        zip: ''
      }*/,
    });
  }
}
