import {FormMainComponent} from "./form-main/form-main.component";
import {ReaciveForm2Component} from "./reacive-form2/reacive-form2.component";
import {TemplateFormComponent} from "./template-form/template-form.component";
import {HeroDetailComponent} from "./hero-detail/hero-detail.component";
import {HeroListComponent} from "./hero-list/hero-list.component";
/**
 * Created by Administrator on 2017/8/2.
 */
export const formRoutes = [
  {
    path: '',
    component: FormMainComponent,
    // canActivate: [AuthGuard],
    children: [
      { path: 'form2', redirectTo: 'formComponent', pathMatch: 'full'},
      { path: 'form3', component: ReaciveForm2Component },
      { path: 'form4', component: TemplateFormComponent },
      { path: 'form5', component: HeroDetailComponent },
      { path: 'form6', component: HeroListComponent },
    ]
  }
]
