import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {News2Component} from "../../news2/news2.component";
import {RouterModule} from "@angular/router";
import {productRoutes} from "app/product/product/product.router";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(productRoutes)
  ],
  declarations: []
})
export class ProductModule { }
