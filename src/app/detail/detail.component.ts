import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  private id: number;
  constructor(private routeInfo:  ActivatedRoute) { }

  ngOnInit() {
    this.routeInfo.params.subscribe((params2:  Params) => this.id = params2['id']);
  }

}
