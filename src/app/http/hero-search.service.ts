import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";
import {Http} from "@angular/http";
import {Hero} from "./mock-heroes";

@Injectable()
export class HeroSearchService {
  heroes: Observable<Hero[]>;
  private searchTerms = new Subject<string>();
  constructor( private http: Http) { }
  search(term: string): Observable<Hero[]> {
    return this.http
      .get(`app/heroes/?name=${term}`)
      .map(response => response.json().data as Hero[]);
  }

}
