import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpMainComponent } from './http-main.component';

describe('HttpMainComponent', () => {
  let component: HttpMainComponent;
  let fixture: ComponentFixture<HttpMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HttpMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HttpMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
