import { Component, OnInit } from '@angular/core';
import {Http} from "@angular/http";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import {Router} from "@angular/router";

@Component({
  selector: 'app-http-main',
  templateUrl: './http-main.component.html',
  styleUrls: ['./http-main.component.css']
})
export class HttpMainComponent implements OnInit {
  apiUrl = 'https://jsonplaceholder.typicode.com/users';
  username: string = '';
  user: any;
  isShowforkJoin: boolean = false;
  constructor(private http: Http,
              private route: Router) { }

  ngOnInit() {
    /*this.http.get('https://jsonplaceholder.typicode.com/users')
      .map(res => res.json())
      .subscribe(users => console.log(users));*/

  /*  this.http.get(this.apiUrl)
     .map(res => res.json())
     .subscribe(users => {
     let username = users[6].username;
     this.http.get(`${this.apiUrl}?username=${username}`)
     .map(res => res.json())
     .subscribe(
     user => {
     this.username = username;
     this.user = user;
     });
     });*/
  // 我们通过 mergeMap 操作符，解决了嵌套订阅的问题。最后我们来看一下如何处理多个并行的 Http 请求。
    this.http.get(this.apiUrl)
      .map(res => res.json())
      .mergeMap(users => {
        this.username = users[6].username;
        return this.http.get(`${this.apiUrl}?username=${this.username}`)
          .map(res => res.json())
      })
      .subscribe(user => this.user = user);
  }

  showforkJoin() {
    this.isShowforkJoin = true;
    this.route.navigate(['/forkJoin']);
  }

  showhttpmodule() {
    this.isShowforkJoin = !this.isShowforkJoin ;
    this.route.navigate(['/httpClient']);
  }

  showappmemeber() {
     this.isShowforkJoin = !this.isShowforkJoin ;
     this.route.navigate(['/member']);
  }
}
