import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import * as _ from 'lodash';
import "rxjs/add/operator/do";

interface Course {
  description: string;
  courseListIcon:string;
  iconUrl:string;
  longDescription:string;
  url:string;
}
@Component({
  selector: 'app-http-client',
  templateUrl: './http-client.component.html',
  styleUrls: ['./http-client.component.css']
})
export class HttpClientComponent implements OnInit {
  courses$: Observable<any>;
  courses2$: Observable<any>;
  courses3$: Observable<any>;
  pageUrl: string = 'https://angular-http-guide.firebaseio.com';
  constructor(private http: HttpClient) { }

  ngOnInit() {
    /*this.courses$ = this.http
      .get("https://angular-http-guide.firebaseio.com/courses.json")
      .map(data => _.values(data)).do(console.log);*/


    const params = new HttpParams()
      .set('orderBy', '"$key"')
      .set('limitToFirst', "1");

    this.courses$ = this.http
      .get(this.pageUrl + '/courses.json', {params})
      .do(console.log)
      .map(data => _.values(data))

    const params2 = new HttpParams({fromString: 'orderBy="$key"&limitToFirst=1'});

    this.courses2$ = this.http
      .request(
        "GET",
        this.pageUrl + "/courses.json",
        {
          responseType:"json",
          params
        })
      .do(console.log)
      .map(data => _.values(data));

    const headers = new HttpHeaders().set("X-CustomHeader", "custom header value");
    // 设置 HTTP Headers
    this.courses3$ = this.http
      .get(
        this.pageUrl + "/courses.json",
        {headers})
      .do(console.log)
      .map(data => _.values(data));

  }

}
