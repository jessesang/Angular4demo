import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {httpRoutes} from "./http.router";
import {RouterModule} from "@angular/router";
import {HttpModule} from "@angular/http";
import {HttpMainComponent} from "./http-main/http-main.component";
import { ForkJoinComponent } from './fork-join/fork-join.component';
import {HttpClientModule} from "@angular/common/http";
import { HttpClientComponent } from './http-client/http-client.component';
import {MemberService} from "./member.service";
import { MemberComponent } from './member/member.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import {FormsModule} from "@angular/forms";
import {InMemoryDataService} from "./in-memory-data.service";
import {InMemoryWebApiModule} from "angular-in-memory-web-api";
import {HeroService} from "./hero.service";
import { HttpMain2Component } from './http-main2/http-main2.component';
import { HeroSearchComponent } from './hero-search/hero-search.component';
import { HeroesComponent } from './heroes/heroes.component';
import {HeroSearchService} from "./hero-search.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    RouterModule.forChild(httpRoutes)
  ],
  providers: [MemberService, HeroService, HeroSearchService],
  declarations: [HttpMainComponent, ForkJoinComponent, HttpClientComponent,
    MemberComponent, DashboardComponent, HeroDetailComponent, HttpMain2Component, HeroSearchComponent, HeroesComponent]
})
export class HttpNewModule { }
