import { Component, OnInit } from '@angular/core';
import {MemberService} from "../member.service";

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.css']
})
export class MemberComponent implements OnInit {
  members: Object;
  constructor(private memberService: MemberService) { }

  ngOnInit() {
    this.memberService.getMembers()
      .subscribe(data => {
        console.log('data', data);
        if (data) this.members = data;
      });
  }

}
