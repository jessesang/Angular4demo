import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpMain2Component } from './http-main2.component';

describe('HttpMain2Component', () => {
  let component: HttpMain2Component;
  let fixture: ComponentFixture<HttpMain2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HttpMain2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HttpMain2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
