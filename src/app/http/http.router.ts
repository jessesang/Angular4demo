import {HttpMainComponent} from "./http-main/http-main.component";
import {ForkJoinComponent} from "./fork-join/fork-join.component";
import {HttpClientComponent} from "./http-client/http-client.component";
import {MemberComponent} from "./member/member.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {HeroDetailComponent} from "./hero-detail/hero-detail.component";
import {HttpMain2Component} from "./http-main2/http-main2.component";
import {HeroesComponent} from "./heroes/heroes.component";
/**
 * Created by Administrator on 2017/8/7.
 */
export const httpRoutes = [
  {
    path: '',
    component: HttpMainComponent,
    // canActivate: [AuthGuard],
    children: [
      { path: 'http2', redirectTo: 'HttpMainComponent', pathMatch: 'full'},
      { path: 'forkJoin', component: ForkJoinComponent},
      { path: 'httpClient', component: HttpClientComponent},
      { path: 'member', component: MemberComponent},
      { path: 'detail/:id', component: HeroDetailComponent },
      { path: 'HttpMain2', component: HttpMain2Component, children: [
        { path: 'heroes',     component: HeroesComponent },
        { path: 'dashboard',  component: DashboardComponent }] },
    ]
  }
]
