import {DiMainComponent} from "./di-main/di-main.component";
import {Question1Component} from "./question1/question1.component";
import {HeroComponent} from "../di/hero/hero.component";
import {Product1Component} from "./product1/product1.component";
/**
 * Created by Administrator on 2017/7/28.
 */
export const di2Routes = [
  {
    path: '',
    component: DiMainComponent,
    // canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'q1', pathMatch: 'full'},
      { path: 'q1/:runname', component: Question1Component },
      { path: 'hero', component: HeroComponent },
      { path: 'product', component: Product1Component },
      /*{ path: 'usertable/page/:page', component: UserTableComponent },
      { path: 'usertable/edituser/:userId', component: UserProfileComponent },
      { path: 'usertable/newuser', component: UserProfileComponent },
      { path: 'sysparam', component: SysParamComponent },
      { path: '**', redirectTo:'posttable/page/1' }*/
    ]
  }
];
