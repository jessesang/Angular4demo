import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {di2Routes} from "./di2.routes";
import {RouterModule} from "@angular/router";
import { Question1Component } from './question1/question1.component';
import {DiMainComponent} from "./di-main/di-main.component";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {HeroComponent} from "../di/hero/hero.component";
import {HeroService} from "../di/hero.service";
import {LoggerService} from "../di/logger.service";
import {MockHeroService} from "../di/mock-hero.service";
import {Product2Component} from "./product2/product2.component";
import {Product1Component} from "./product1/product1.component";
import {AnotherProductService} from "./share/another-product.service";
import {ProductService} from "./share/product.service";

/*() => {
 return new LoggerService(true);
 }*/
export function authFactory() {
   return new LoggerService(true);
}
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    RouterModule.forChild(di2Routes)
  ],
  declarations: [Question1Component, DiMainComponent, HeroComponent, Product2Component, Product1Component],
  providers: [{
    provide: HeroService,   useClass:  MockHeroService
  }, {
    provide: LoggerService,
    useFactory: authFactory}, /*{
    provide: ProductService,
    useFactory: (logger: LoggerService, appConfig) => {
      if (appConfig.isDev) {
        return new ProductService(logger);
      } else {
        return new AnotherProductService(logger);
      }
    }/!*, deps: [LoggerService, 'APP_CONFIG']*!/
  }*/],
  exports: [DiMainComponent]
})
export class Di2Module { }
