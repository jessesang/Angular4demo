import { Injectable } from '@angular/core';
import {Product} from "./product.service";
import {LoggerService} from "../../di/logger.service";

@Injectable()
export class AnotherProductService {
  getProduct(): Product {
    return new Product(0,"三星", 15899, "最新款三星手机");
  }
  constructor(public logger: LoggerService) { }

}
