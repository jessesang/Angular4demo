import { Injectable } from '@angular/core';
import {LoggerService} from "../../di/logger.service";


@Injectable()
export class ProductService {
  getProduct(): Product {
    this.logger.log("getProduct方法被调用");
    return new Product(0,"iphone7", 5899, "最新款苹果手机")
  }
  constructor(public logger: LoggerService) { }

}
export class Product {
  constructor(
    public id: number,
    public title: string,
    public price: number,
    public  desc: string
  ) {

  }
}
