import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";

@Component({
  selector: 'app-question1',
  templateUrl: './question1.component.html',
  styleUrls: ['./question1.component.css']
})
export class Question1Component implements OnInit {
  runName: string;
  constructor(private routeInfo: ActivatedRoute) { }

  ngOnInit() {
     this.runName = this.routeInfo.snapshot.params["runname"];
    // this.routeInfo.params.subscribe((param2: Params) => {
    //   console.log('param2',param2);
    //   this.runName = param2["runname"]
    // });
    console.log(this.runName)
  }

}
