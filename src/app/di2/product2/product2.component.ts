import { Component, OnInit } from '@angular/core';
import {ProductService} from "../share/product.service";
import {AnotherProductService} from "../share/another-product.service";

@Component({
  selector: 'app-product2',
  templateUrl: './product2.component.html',
  styleUrls: ['./product2.component.css'],
   providers: [{
   provide: ProductService, useClass: AnotherProductService
   }]
})
export class Product2Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
