import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiMainComponent } from './di-main.component';

describe('DiMainComponent', () => {
  let component: DiMainComponent;
  let fixture: ComponentFixture<DiMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
