import {Component, Injectable, OnInit, ReflectiveInjector} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-di-main',
  templateUrl: './di-main.component.html',
  styleUrls: ['./di-main.component.css']
})
export class DiMainComponent implements OnInit {
  car: Car;
   message: string;
  message2: string;
  message0: string;
  constructor(private router: Router) { }

  ngOnInit() {
    // let car = new Car(); // 造辆新车
    // car.run(); // 开车上路咯

    const engine = new NewEngine();
     const body = new Body();
     const doors = new Doors();
     this.car = new Car(engine, body, doors);
     this.message0 =  this.car.run();

    // let injector2 = ReflectiveInjector.resolveAndCreate([Car2,
    //   Engine, Doors, Body]);
    let injector = ReflectiveInjector.resolveAndCreate([Car2,
      Engine, Doors, Body]);
    // let car1 = injector.get(Car);
    let car = injector.get(Car2);
    this.message2 = car.run();
 //   this.message = car.run();
  }

  question0Click() {
    console.log('this.message0', this.message0);
    this.router.navigate(['/di/q1', this.message0]);
  }

  question1Click() {
    console.log('this.message2', this.message2);
    this.router.navigate(['/di/q1', this.message2]);
  }

  question2Click() {
    this.router.navigate(['/di/q1',"dadad"]);
  }
}
export class  Body { }

export class  Doors { }

export  class Engine {
  start() {
    console.log('🚗开动鸟~dd~~');
    return  'Engine22开动了';
  }
}

export  class NewEngine {
  start() {
    console.log('🚗开动鸟~222222~~');
    return  'NewEngine开动了';
  }
}

export  class Car {
 engine: Engine;
 doors: Doors;
 body: Body;

 /*constructor() {
 this.engine = new Engine();
 this.body = new Body();
 this.doors = new Doors();
 }*/
 constructor(engine, body, doors) {
 this.engine = engine;
 this.body = body;
 this.doors = doors;
 }

 run() {
  return this.engine.start();
 }
 }
@Injectable()
export  class Car2 {
  constructor(
    private engine: Engine,
    private body: Body,
    private doors: Doors) {}

  run() {
   return  this.engine.start();
  }
};
