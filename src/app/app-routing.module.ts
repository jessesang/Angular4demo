/**
 * Created by Administrator on 2017/7/19.
 */
import { RouterModule, Routes } from '@angular/router';
import {NgModule} from "@angular/core";
import {NewsComponentComponent} from "./news-component/news-component.component";
import {DetailComponent} from "./detail/detail.component";
import {News2Component} from "./news2/news2.component";
import {HomeComponent} from "./home/home.component";
import {ChatComponent} from "./chat/chat.component";
import {ProductDescComponent} from "./product-desc/product-desc.component";
import {ProductComponent} from "./product/product.component";
import {SellerInfoComponent} from "./seller-info/seller-info.component";
import {LoginGuard} from "./guard/login.guard";
import {UnsavedGuard} from "./guard/unsaved.guard";
import {BindbothComponent} from "./bindboth/bindboth.component";
import {MainDirective2Component} from "./directive/main-directive2/main-directive2.component";
import {FormMainComponent} from "./form/form-main/form-main.component";


/* pathMatch?: string; 默认为前缀匹配 "prefix"; "full" 为完全匹配
outlet?: string; 路由目标
children?: Routes; 子路由的规则*/
const routes: Routes = [
  // 以后在这里改动配置,
  {
    path: 'di2',
    loadChildren: './di/di/di.module#DiModule'
  },
  {
    path: 'di',
    loadChildren: './di2/di2.module#Di2Module'
  },
  {
    path: 'bind',
    component:  BindbothComponent
  },
  {
    path: 'componentConcat',
    loadChildren:'./zstest/zstest.module#ZstestModule'
  },
  {
    path: 'directive',
    loadChildren: './directive/direct.module#DirectModule'
  },
  {
    path: 'form',
    loadChildren: './form/form-module.module#FormModuleModule'
  },
  {
    path: 'http_demo',
    loadChildren: './http/http.module#HttpNewModule'
  },
  {
    path: 'lifeCycle',
    loadChildren: './life-cycle/main/lift-cycle.module#LifeCycleModule'
  }
  /*{
    path: 'di',
    component: MainComponent
  }*/
  // path:路径 /detail/1 component:组件
  /*{ path: 'detail/:id', component: DetailComponent },
  // 懒加载子模块, 子模块需要配置路由设置启动子组件


  {path: 'home', component: HomeComponent},
  {path: 'chat', component: News2Component, outlet: 'aux'},
  {path: 'product/:id', component: ProductComponent, children : [
    { path: '', component: ProductDescComponent},
    {path: 'seller/:id', component: SellerInfoComponent}], canActivate: [LoginGuard], canDeactivate: [UnsavedGuard]},
// 重定向
  { path: '**', pathMatch: 'full', redirectTo: '/home' }, // 调换这个顺序 影响路由的匹配*/
];
/*// 将路由配置导出为 appRouting 以供调用, 子模块中的路由使用 forChild 而不是 forRoot
export const appRouting = RouterModule.forRoot(routes);*/
@NgModule({
   imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [LoginGuard, UnsavedGuard]
})
export class  AppRoutingModule { }
