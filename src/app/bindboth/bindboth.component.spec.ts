import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindbothComponent } from './bindboth.component';

describe('BindbothComponent', () => {
  let component: BindbothComponent;
  let fixture: ComponentFixture<BindbothComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindbothComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindbothComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
