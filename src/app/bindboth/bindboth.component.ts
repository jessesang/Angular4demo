import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bindboth',
  templateUrl: './bindboth.component.html',
  styleUrls: ['./bindboth.component.css']
})
export class BindbothComponent implements OnInit {
  user: { username: string };
  username: string;
  constructor() { }

  ngOnInit() {
    this.user = { username: 'Semlinke r' };
  }

}
