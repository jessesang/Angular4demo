import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { TestmapComponent } from './testmap/testmap.component';
import { BindbothComponent } from './bindboth/bindboth.component';
import {FormsModule} from "@angular/forms";
import {AppRoutingModule} from "./app-routing.module";
import { NewsComponentComponent } from './news-component/news-component.component';
import { DetailComponent } from './detail/detail.component';
import { News2Component } from './news2/news2.component';
import { HomeComponent } from './home/home.component';
import { ChatComponent } from './chat/chat.component';
import { ProductComponent } from './product/product.component';
import { ProductDescComponent } from './product-desc/product-desc.component';
import { SellerInfoComponent } from './seller-info/seller-info.component';
// import { MainComponent } from './di/main/main.component';
import { MainAppComponent } from './di/main-app/main-app.component';
import { HeroComponent } from './di/hero/hero.component';
// import {HeroService} from "./di/hero.service";
import {LoggerService} from "./di/logger.service";
import {DiModule} from "./di/di/di.module";
import {ProductModule} from "./product/product/product.module";
import { ProductInfoComponent } from './di/product-info/product-info.component';
import { DiMainComponent } from './di2/di-main/di-main.component';
import {Di2Module} from "./di2/di2.module";
import {HttpModule} from "@angular/http";
import {ZstestModule} from "./zstest/zstest.module";
import { CreateDirectiveComponent } from './directive/create-directive/create-directive.component';
import {MainDirective2Component} from "./directive/main-directive2/main-directive2.component";
import {DirectModule} from "./directive/direct.module";
import { FormMainComponent } from './form/form-main/form-main.component';
import {FormModuleModule} from "./form/form-module.module";
import { HttpMainComponent } from './http/http-main/http-main.component';
import {HttpNewModule} from "./http/http.module";
import { PeekABooComponent } from './life-cycle/peek-a-boo/peek-a-boo.component';
import { PeekAParentComponent } from './life-cycle/peek-a-parent/peek-a-parent.component';
import { MainComponent } from './life-cycle/main/main.component';
import {LifeCycleModule} from "./life-cycle/main/lift-cycle.module";


@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    TestmapComponent,
    BindbothComponent,
    // MainComponent,
     // NewsComponentComponent,
    DetailComponent,
    // News2Component,
    // HomeComponent,
    ChatComponent,
    // ProductComponent,
    // ProductDescComponent,
    // SellerInfoComponent,
    MainAppComponent,
    //  HeroComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    DiModule,
    ProductModule,
    Di2Module,
    ZstestModule,
    DirectModule,
    FormModuleModule,
    HttpNewModule,
    LifeCycleModule
  ],
  providers: [/*{
    provide: HeroService, useClass: MockHeroService
  },HeroService, {
    provide: LoggerService,
    useFactory: () => {
      return new LoggerService(true);
    }
  }*/
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
